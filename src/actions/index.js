import jsonPlaceholder from '../api/jsonPlaceholder'
import _ from 'lodash';

// Call action creator inside action creator
export const fetchUserAndPosts = () => async (dispatch, getState) => {

    await dispatch(fetchPosts());
    // console.log(getState().posts)
    const userIds = _.uniq(_.map(getState().posts, 'userId'));
    userIds.forEach(id => {
        dispatch(fetchUser(id))
    })
}


export const fetchPosts = () => async (dispatch) => {
        const responce = await jsonPlaceholder.get('/posts');        
        dispatch({
            type: 'FETCH_POSTS', payload : responce.data
        })
};


export const fetchUser = (id) => async (dispatch) => {
    const responce = await jsonPlaceholder.get(`/users/${id}`);
    console.log(responce.data);
    dispatch({
        type: 'FETCH_USER', payload: responce.data
    });
};
